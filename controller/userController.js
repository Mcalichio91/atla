module.exports = function(app)  {

    const userService = require("../services/userService")
    const Cors = require("./../config/cors")
    const authorizationService = require ('../services/authService.js');
    app.use(Cors.cors(Cors.corsOptions));
    const urlBase = "/usuarios";

    app.post(urlBase+'/login' ,async (req,res) => {
		console.log(req.body)
		let authResponse = await userService.userLogin(req.body.email);
        res.send(authResponse)
    })

    app.post(urlBase+"/crear", async function (req, res) {
            let data = req.body
            console.log(data)
            const userServiceResponse = await userService.userCreate(data)
            res.status(200).send(userServiceResponse)
     })
    
     app.delete(urlBase+"/borrar/:id", async function (req, res) {
            let id = req.params.id
            const userServiceResponse = await userService.userDelete(id)
            res.status(200).send(userServiceResponse)
     })
     
    app.put(urlBase+"/editar/:id", async function (req, res) {
            let id = req.params.id
            let data = req.body
            const userServiceResponse = await userService.userUpdate(data, id)
            res.status(200).send(userServiceResponse)
     })

    app.get(urlBase+"/all", async function (req, res) {
            const getAllUsersResponse = await userService.getAllUsers()
            res.status(200).send(getAllUsersResponse)
     })
}