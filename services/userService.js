const { response } = require('express');
const jwt = require("jsonwebtoken");
const Conn = require('../config/conn');

const UserService = {
    userLogin: (email, password) => {

        if(email){
            const token 	= jwt.sign({name: email},conn.key, {expiresIn: 7200});
            response.body 		= {response: "Ok", token: token};
            response.status(200)
            return response;
        }
        
        response.body       = {error: "Usuario / Contraseña incorrectos."}
        response.status(401)
        return response; 
    },
    getAllUsers: async function () {
       let sql = 'SELECT * FROM usuarios'
       let query = await conn.query(sql)

        let response = {result: query}        
        
        return response
    },
    userCreate: async function (usuarioNuevo) {

        let sql = `INSERT INTO usuarios (name, lastname, password) VALUES ("${usuarioNuevo.name}", "${usuarioNuevo.lastname}", "${usuarioNuevo.password}")`
        let response = "No se pudo crear el usuario"
        
        try {
            let query = await conn.query(sql)
            response = {result: query}
            
        } catch (error) {
            response = {error: error}
        }
        
        return response
    },
    userUpdate: async function (usuarioUpdate, id) {
        console.log(usuarioUpdate, id)
        let sql = `UPDATE usuarios SET 
        name = '${usuarioUpdate.name}', 
        lastname = '${usuarioUpdate.lastname}', 
        password = '${usuarioUpdate.password}'
        WHERE id = '${id}'`
        let response = "No se pudo crear el usuario"

        try {
            let query  = conn.query(sql)
            response = {result:  `Usuario id ${id} editado corretamente`}
        } catch (error) {
            response = {error: "Error : ", error}
        }

        return response
    },


    userDelete: async function (id) {

        let sql = `DELETE FROM usuarios WHERE id = '${id}'`

        let response = "No se pudo borrar el usuario"

        try {
            conn.query(sql)
            response = {result:  `Usuario id ${id} borrado corretamente`}
        } catch (error) {
            response = {error: "Error : ", error}
        }

        return response
    },

}

module.exports = UserService;