const mysql      	= require('mysql');
const connection 	= mysql.createConnection({
	// host     : process.env.DB_HOST,
	// user     : process.env.DB_USER,
	// password : process.env.DB_PASSWORD,
	// port	 : process.env.DB_PORT,
	// database : process.env.DB_NAME
	host     : "localhost",
	user     : "root",
	password : "root",
	port	 : 8889,
	database : "ecommerce"
});

connection.connect(function(err){
	if (err) {
		console.error("Error al conectar a Data Base ::", err.stack);
		return;
	}
	console.log("Conectado a Data Base con Id. :: ", connection.threadId)
});
 
let query = (sql) => {
	return new Promise( (resolve, reject) => {
		connection.query(`${sql}`, function (error, results, fields) {
		  if (error) {
		  	resolve(JSON.parse(JSON.stringify(error)));
		  }else {
		  	resolve(results);
		  }
		});
	})
}

let Conn = {
	query: query,
	key: "MILLAVESECRETA"
}

module.exports = Conn