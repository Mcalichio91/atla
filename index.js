const express = require('express');
const app = express();
const jwt = require('jsonwebtoken');
const bodyParser = require("body-parser");

app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));

require('./controller/userController.js')(app);


global.conn 		= require('./config/conn');
global.Cors 		= require('./config/cors.js');


app.use(bodyParser.json())

app.get("/*", function(_,res){
    res.status(200).send("hola")
})

app.listen(3000, console.log("servidor ejecutando en puerto 3000"));